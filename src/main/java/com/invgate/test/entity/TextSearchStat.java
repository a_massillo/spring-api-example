package com.invgate.test.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * @author amassillo
 *
 */
@Data
@Entity
@Table(name="text_search_stats")
public class TextSearchStat {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private Integer helpdeskId;
	
	private Integer searchCnt;
	
	private String searchText;
	
	private LocalDateTime createDate;
	
	private LocalDateTime updateDate;
	
	public TextSearchStat() {}
	
	public TextSearchStat(Integer pHelpdeskId, String pSearchText) {
		this.helpdeskId = pHelpdeskId;
		this.searchText = pSearchText;
		this.searchCnt = 0;
	}
}
