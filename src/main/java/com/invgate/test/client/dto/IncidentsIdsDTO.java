package com.invgate.test.client.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

/**
 * 
 * @author amassillo
 *
 */
@JacksonXmlRootElement
@Data
public class IncidentsIdsDTO {
	private String status;
	private String info;
	private Integer[] requestIds;
}
