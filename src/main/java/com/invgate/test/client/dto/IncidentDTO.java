package com.invgate.test.client.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

/**
 * 
 * @author amassillo
 *
 */
@JacksonXmlRootElement
@Data
public class IncidentDTO {


    private Long id;
    private String title;
    private Integer category_id;
    private String description;
    private Integer priority_id;
    private Long user_id;
    private Long creator_id;
    private Long assigned_id;
    private Long  assigned_group_id;
    private Long date_ocurred;
    private Long source_id;
    private Integer status_id;
    private Integer type_id;
    private Long created_at;
    private Long last_update;
    private Integer process_id;
    private Long solved_at;
    private Long closed_at;
    private String closed_reason;
    private Boolean data_cleaned;
    private String[] attachments;
    
    public IncidentDTO() {}
    public IncidentDTO(Long pId) {
    	this.setId(pId);
    }
}
