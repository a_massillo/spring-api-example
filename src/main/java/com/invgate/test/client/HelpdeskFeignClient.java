package com.invgate.test.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.invgate.config.HelpdeskFeignClientConfiguration;
import com.invgate.test.client.dto.IncidentDTO;
import com.invgate.test.client.dto.IncidentsIdsDTO;

import feign.Headers;

/**
 * 
 * @author amassillo
 *
 */
@FeignClient(name = "helpdesk-service-client", configuration=HelpdeskFeignClientConfiguration.class,
			 url="${helpdesk-service.ribbon.listOfServers}")
@Headers("Accept: application/json")
public interface HelpdeskFeignClient {
	
	/**
	 * find list of incident ids by helpdesk_id
	 * @param pHelpdeskId
	 * @return
	 */
	 @GetMapping (value = "/incidents.by.helpdesk")
	 public IncidentsIdsDTO getByHelpdeskId(@RequestParam("helpdesk_id") Integer pHelpdeskId);
	
	 /**
	  * find specific incident
	  * @param pIncidentId
	  * @return
	  */
	 @GetMapping (value = "/incident")
	 public IncidentDTO getByIncidentId(@RequestParam("id") Integer pIncidentId);
}
