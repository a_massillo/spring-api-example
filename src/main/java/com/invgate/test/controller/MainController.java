package com.invgate.test.controller;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invgate.test.client.dto.IncidentDTO;
import com.invgate.test.entity.TextSearchStat;
import com.invgate.test.service.SearchService;
import com.invgate.test.service.exception.HelpDeskIdNotFoundException;


/**
 * 
 * @author amassillo
 *
 */
@RestController
@RequestMapping("/search")
public class MainController {
	
	Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	private SearchService service;
	
	/**
	 * 
	 * @param pHelpdeskId
	 * @param pTextToSearch
	 * @param pDetailed
	 * @return
	 */
	@GetMapping (value = "/incidents")
	public ResponseEntity <List<IncidentDTO>> get(@RequestParam("helpdesk_id") Integer pHelpdeskId,
												  @RequestParam(value="text_to_search", required = false) String pTextToSearch,
												  @RequestParam(value="detailed", required=false) Boolean pDetailed){
		try {
			//return found results
			return new ResponseEntity<List<IncidentDTO>> (service.search(pHelpdeskId, pTextToSearch, pDetailed),
														  HttpStatus.OK);
		}catch (HelpDeskIdNotFoundException e) {
			//not found
			return new ResponseEntity<List<IncidentDTO>> (HttpStatus.NOT_FOUND);
		}catch (Exception e) {
			//other unexpected error
			return new ResponseEntity<List<IncidentDTO>> (HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param pHelpdeskId
	 */
	@GetMapping (value = "/searchTop")
	public ResponseEntity<List<TextSearchStat>> getTop(@RequestParam(value="helpdesk_id", required=false) Integer pHelpdeskId){
		try {
			return new ResponseEntity<List<TextSearchStat>> (service.getTop(pHelpdeskId),HttpStatus.OK);
		}catch (Exception e) {
			//other unexpected error
			return new ResponseEntity<List<TextSearchStat>> (HttpStatus.BAD_REQUEST);
		}
	}
	
	
}
