package com.invgate.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.invgate.test.entity.TextSearchStat;

/**
 * 
 * @author amassillo
 *
 */
@Repository
public interface IncidentRepository extends JpaRepository<TextSearchStat, Integer> {
	
	/**
	 * 
	 * @param pHelpdeskId
	 * @return
	 */
	public List<TextSearchStat> findTop5ByHelpdeskIdOrderBySearchCntDesc(Integer pHelpdeskId);
	
	/**
	 * 
	 * @param pHelpdeskId
	 * @param ptextSearch
	 * @return
	 */
	public TextSearchStat findOneByHelpdeskIdAndSearchText(Integer pHelpdeskId, String ptextSearch);
}
