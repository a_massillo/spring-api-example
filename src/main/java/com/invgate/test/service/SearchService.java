package com.invgate.test.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invgate.test.client.HelpdeskFeignClient;
import com.invgate.test.client.dto.IncidentDTO;
import com.invgate.test.client.dto.IncidentsIdsDTO;
import com.invgate.test.entity.TextSearchStat;
import com.invgate.test.repository.IncidentRepository;
import com.invgate.test.service.exception.HelpDeskIdNotFoundException;

/**
 * 
 * @author amassillo
 *
 */
@Service
public class SearchService {
	
	Logger logger = LoggerFactory.getLogger(SearchService.class);
	
	@Autowired
	private IncidentRepository repository;
	
	@Autowired
	private HelpdeskFeignClient helpDesjClient;
	
	/**
	 * 
	 * @param pHelpdeskId
	 * @param pTextToSearch
	 * @param pDetailed
	 * @return
	 * @throws HelpDeskIdNotFoundException
	 */
	public List<IncidentDTO> search(Integer pHelpdeskId, String pTextToSearch, Boolean pDetailed) throws HelpDeskIdNotFoundException{
		List<IncidentDTO> lResponse 	   = new ArrayList<IncidentDTO>();
		IncidentsIdsDTO   lServiceResponse = helpDesjClient.getByHelpdeskId(pHelpdeskId);
		//id not found
		if (lServiceResponse == null) throw new HelpDeskIdNotFoundException();
		
		for (Integer l: lServiceResponse.getRequestIds()) {
			IncidentDTO lIncident = helpDesjClient.getByIncidentId(l);
			//if incident detail not found, we warn via log
			if (lIncident == null) {
				logger.warn("incident {} not found",l);
				continue;
			}
			//not text search provided
			if (StringUtils.isNotBlank(pTextToSearch) && !StringUtils.contains(lIncident.getDescription(),pTextToSearch)) {
				continue;
			}
			//map according detailed info
			//if other info should be mapped, i might use mapstruct
			lResponse.add(pDetailed==null || !pDetailed ? new IncidentDTO(lIncident.getId()):lIncident);
		}	
		return lResponse;
	}
	
	/**
	 * 
	 * @param pHelpdeskId
	 * @param pSearchText
	 */
	public void saveStats(Integer pHelpdeskId, String pSearchText) {
		//check if there's some record in db, to sumup current search to that stats
		TextSearchStat lStat = this.repository.findOneByHelpdeskIdAndSearchText(pHelpdeskId, pSearchText);
		
		if (lStat == null)
			lStat = new TextSearchStat(pHelpdeskId, pSearchText);
		
		lStat.setSearchCnt(lStat.getSearchCnt() + 1);
		this.repository.save(lStat);
	}
	
	/**
	 * 
	 * @param pHelpdeskId
	 * @return
	 */
	public List<TextSearchStat> getTop(Integer pHelpdeskId) {
		return this.repository.findTop5ByHelpdeskIdOrderBySearchCntDesc(pHelpdeskId);
	}
}
