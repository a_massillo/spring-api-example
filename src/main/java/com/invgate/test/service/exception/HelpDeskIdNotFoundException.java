package com.invgate.test.service.exception;

/**
 * 
 * @author amassillo
 *
 */
public class HelpDeskIdNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
