package com.invgate.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.FilterType;

/**
 * 
 * @author amassillo
 *
 */
@SpringBootApplication
@EnableFeignClients   (basePackages = "com.invgate.test.client")
@EntityScan			  (basePackages = "com.invgate.test.entity")
@EnableJpaRepositories(basePackages = "com.invgate.test.repository")
@ComponentScan(basePackages = "com.invgate.test", 
			   excludeFilters = @Filter(type=FilterType.ASSIGNABLE_TYPE,classes= HelpdeskFeignClientConfiguration.class))
public class Application {
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}