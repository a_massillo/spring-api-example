package com.invgate.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import feign.auth.BasicAuthRequestInterceptor;

/**
 * 
 * @author amassillo
 *
 */
@Configuration
public class HelpdeskFeignClientConfiguration {

	@Value( "${helpdesk-service.client.user}" )
	private String username;
	
	@Value( "${helpdesk-service.client.password}" )
	private String password;
	
	public class FeignClientConfiguration {    
		@Bean
	    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
	         return new BasicAuthRequestInterceptor(username,password);
	    }
	}
}
